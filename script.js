function Traveler(name) {
    this.name = name;
    this.food = 1;
    this.isHealthy = true;
}

Traveler.prototype = {
    constructor: Traveler,
    hunt: function () {
        this.food = this.food + 2;
        // console.log(this.name + ' food:', this.food)
    },
    eat: function () {
        if (this.food > 0) {
            this.food = this.food - 1;
            // console.log(`${this.name} food: ${this.food}`);
        } else {
            this.isHealthy = false;
            // console.log(`Is ${this.name} healthy? ${this.isHealthy}`);
        }
    },
};

function Wagon(capacity) {
    Traveler.call(this, name)
    this.capacity = capacity;
    this.passengers = []
}
Wagon.prototype = {
    constructor: Wagon,
    getAvailableSeatCount: function getAvailableSeatCount() {
        return this.capacity - this.passengers.length
    },
    join: function join(travelers) {
        if (this.passengers.length < this.capacity) {
            this.passengers.push(travelers);
        }
    },
    shouldQuarantine: function () {
        // if (!this.passengers.isHealthy) {
        //     return true
        // } else {
        //     return false;
        // }
        for(let i = 0; i < this.passengers.length; i++){
            if(!this.passengers[i].isHealthy){
                return true
            }
        }
        return false
    },
    totalFood: function totalFood() {
        // console.log(this.passengers);
        let totalFood = 0;
        for (i = 0; i < this.passengers.length; i++) {
            totalFood = this.passengers[i].food + totalFood;
        }
        return totalFood;
    }
}

function Doctor(name) {
    Traveler.call(this, name);
}
Doctor.prototype = Object.create(Traveler.prototype);
Doctor.prototype.constructor = Doctor;
Doctor.prototype.heal = function (traveler) {
    // console.log(this);
    if (!traveler.isHealthy) {
        traveler.isHealthy = true;
    }
    // console.log(traveler);
}

function Hunter(name) {
    Traveler.call(this, name);
    this.food = 2;
}
// Hunter.prototype = Object.create(Traveler.prototype);
Hunter.prototype = {
    constructor: Hunter,
    hunt: function hunt() {
        this.food = this.food + 5;
        // console.log(this.name + ' food:', this.food)
    },
    eat: function () {
        if (this.food === 1) {
            this.food = this.food - 1;
            // console.log(`${this.name} food: ${this.food}`);
        } else if (this.food > 1) {
            this.food = this.food - 2;
        } else {
            this.isHealthy = false;
            // console.log(`Is ${this.name} healthy? ${this.isHealthy}`);
        }
    },
    giveFood: function giveFood(traveler, numOfFoodUnits) {
        // Transfers numOfFoodUnits from the hunter to a different traveler. If the hunter has less food than they are being asked to give, then no food should be transferred.
        // console.log(this);

        if (numOfFoodUnits > 0) {
            let hunterFood = this.food - numOfFoodUnits;
            traveler.food = numOfFoodUnits;
            // console.log(traveler);
            this.food = hunterFood;
            // console.log(this);    
        }

    },

}
// Create a wagon that can hold 4 people
let wagon = new Wagon(4);
// Create five travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
console.log('drsmith:', drsmith)
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');
console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);
wagon.join(maude); // There isn't room for her!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);
sarahunter.hunt(); // gets 5 more food
drsmith.hunt();
console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);
henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // juan is now hungry (sick)
console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);
drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);
sarahunter.giveFood(juan, 4);
sarahunter.eat(); // She only has 1, so she eats it and is now sick
console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);
